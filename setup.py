import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="human_resources_module",
    version="0.0.2",
    author="Oskar Myczka",
    author_email="oskar.myczka@onet.pl",
    description="Module for human resorces applications",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/OskarMyczka/lesson6",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
