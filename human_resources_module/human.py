#!/usr/bin/env python
""" Lesson 6 - Oskar Myczka"""

__author__ = 'Oskar Myczka'
__copyright__ = 'Copyright (c) Oskar Myczka'
__email__ = 'oskar.myczka@onet.pl'
__version__ = '1.0'


class Human:
    def __init__(self, name, surname):
        """
        Human class
        :param name: First name
        :param surname: Last name
        """
        self._name = name
        self._surname = surname

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname
