#!/usr/bin/env python
""" Lesson 6 - Oskar Myczka"""

__author__ = 'Oskar Myczka'
__copyright__ = 'Copyright (c) Oskar Myczka'
__email__ = 'oskar.myczka@onet.pl'
__version__ = '1.0'

from human_resources_module.human import Human
import unittest


class HumanTests(unittest.TestCase):
    def test_jan_kowalski(self):
        jan = Human("Jan", "Kowalski")
        self.assertEqual("Jan", jan.name, "There is name other than expected")
        self.assertEqual("Kowalski", jan.surname, "There is surname other than expected")

    def test_anna_nowak(self):
        jan = Human("Anna", "Nowak")
        self.assertEqual("Anna", jan.name, "There is name other than expected")
        self.assertEqual("Nowak", jan.surname, "There is surname other than expected")


if __name__ == "__main__":
    unittest.main()
